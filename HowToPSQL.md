# How to deploy a PSQL service on swarm

First, we have our postgres dir with dump file and the Dockerfile:

    [isx48102233@i11 PostgreSQL]$ ls
    db-dump-labclinic.sql  Dockerfile

Let's create the image with Dockerfile with _docker build_.

    [isx48102233@i11 PostgreSQL]$ docker build -t xdri97/postgresql:cloud .

Now create a docker on deattached mode.

    [isx48102233@i11 PostgreSQL]$ docker run --name psqlserver -p 54320:5432 -e POSTGRES_PASSWORD=jupiter -d xdri97/postgresql:cloud 
    a6f6ea913170b5303c0147d832330c6c30cdaf2d46ef0570d88afbe372b3d63f

    [isx48102233@i11 PostgreSQL]$ docker ps
    CONTAINER ID        IMAGE                     COMMAND                  CREATED             STATUS              PORTS                     NAMES
    a6f6ea913170        xdri97/postgresql:cloud   "docker-entrypoint.sh"   9 minutes ago       Up 9 minutes        0.0.0.0:54320->5432/tcp   psqlserver

Ask to postgresql docker with psql:

    [isx48102233@i11 PostgreSQL]$ psql -h 172.17.0.2 -U docker -d lab_clinic -c "select * from doctors;" 
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

Upload image to docker hub to get it from everywhere (needed to compose file).

    [isx48102233@i11 PostgreSQL]$ docker push xdri97/postgresql:cloud

Now let's use this upload image in our work enviroment, where we are able to make stacks, swarms and machines. First download the image from cloud if you have a test with same name like in my case.
If not, just ignore the first command:

    [root@i24 ~]# docker pull xdri97/postgresql:cloud

    [root@i24 ~]# docker run --name psqlserver -h psqlserver -p 5432:5432 -e POSTGRES_PASSWORD=jupiter -d xdri97/postgresql:cloud
    b68ea9588aa497ee4c0c2cbd950200d94e0edbbf26e1c3f29b2a485810072648

    [root@i24 ~]# docker ps
    CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS              PORTS                       NAMES
    b68ea9588aa4        xdri97/postgresql:cloud    "docker-entrypoint..."   6 seconds ago       Up 5 seconds        0.0.0.0:5432->5432/tcp      psqlserver

Comprove asking the server:

    [root@i24 ~]# psql -h 172.17.0.4 -U docker -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

Ask also the local machine IP:

    [root@i24 ~]# psql -h 192.168.2.54 -U docker -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

Let's stack now: (Create a docker-compose file). Init the swarm first.

    [root@i24 ~]# docker swarm init
    Swarm initialized: current node (o40nrc4j3waeal7mpr1xf74qv) is now a manager.

Now create the stack:

    [root@i24 ~]# docker stack deploy -c docker-compose-PSQL.yml psqlstack
    Creating network psqlstack_psqlnet
    Creating service psqlstack_visualizer
    Creating service psqlstack_psql

    [root@i24 ~]# docker stack ls
    NAME                SERVICES
    psqlstack           2

    [root@i24 ~]# docker stack ps psqlstack 
    ID                  NAME                     IMAGE                             NODE                                   DESIRED STATE       CURRENT STATE            ERROR               PORTS
    jbds059tzt1l        psqlstack_psql.1         xdri97/postgresql:cloud           i24.informatica.escoladeltreball.org   Running             Running 13 seconds ago                       
    znzdlrvqisl8        psqlstack_visualizer.1   dockersamples/visualizer:stable   i24.informatica.escoladeltreball.org   Running             Running 16 seconds ago                       
    th523bfv9t9f        psqlstack_psql.2         xdri97/postgresql:cloud           i24.informatica.escoladeltreball.org   Running             Running 13 seconds ago                       
    z9le8at7p1zj        psqlstack_psql.3         xdri97/postgresql:cloud           i24.informatica.escoladeltreball.org   Running             Running 13 seconds ago                       

    [root@i24 ~]# docker service ls
    ID                  NAME                   MODE                REPLICAS            IMAGE                             PORTS
    b7r8z0u52mwr        psqlstack_psql         replicated          3/3                 xdri97/postgresql:cloud           *:5432->5432/tcp
    dq8d3uniuj2o        psqlstack_visualizer   replicated          1/1                 dockersamples/visualizer:stable   *:8080->8080/tcp

Test it:

    [root@i24 ~]# psql -h 192.168.2.54 -U docker -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

    [root@i24 ~]# psql -h i24.informatica.escoladeltreball.org -U docker -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

Remove the stack and leave the swarm:

    [root@i24 ~]# docker stack rm psqlstack 
    Removing service psqlstack_psql
    Removing service psqlstack_visualizer
    Removing network psqlstack_psqlnet

    [root@i24 ~]# docker swarm leave --force
    Node left the swarm.

Now make the stack on swarm with 2 vms:

    [root@i24 ~]# docker-machine create --driver virtualbox mypsqlvm1
    [root@i24 ~]# docker-machine create --driver virtualbox mypsqlvm2
    
Let's talk with who will be the swarm manager "mypsqlvm1"

    [root@i24 ~]# docker-machine ls
    NAME        ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    mypsqlvm1   -        virtualbox   Running   tcp://192.168.99.100:2376           v18.03.1-ce   
    mypsqlvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.03.1-ce   

    [root@i24 ~]# docker-machine env mypsqlvm1
    export DOCKER_TLS_VERIFY="1"
    export DOCKER_HOST="tcp://192.168.99.100:2376"
    export DOCKER_CERT_PATH="/root/.docker/machine/machines/mypsqlvm1"
    export DOCKER_MACHINE_NAME="mypsqlvm1"
    # Run this command to configure your shell: 
    # eval $(docker-machine env mypsqlvm1)

    [root@i24 ~]# eval $(docker-machine env mypsqlvm1)

    [root@i24 ~]# docker-machine ls
    NAME        ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    mypsqlvm1   *        virtualbox   Running   tcp://192.168.99.100:2376           v18.03.1-ce   
    mypsqlvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.03.1-ce   

Start the swarm on vm1 and join with vm2. Show nodes to check status.

    [root@i24 ~]# docker swarm init --advertise-addr 192.168.99.100
    Swarm initialized: current node (hq2fc9aib0atrvhf3yf5gg6ii) is now a manager.
    To add a worker to this swarm, run the following command:
    docker swarm join --token SWMTKN-1-437tkmle44cdhr8atwgdtymol7797dcigdxle6xg535t7d1d83-cualrdhrptlwkp3rhs1hvkm15 192.168.99.100:2377
    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

    [root@i24 ~]# docker-machine ssh mypsqlvm2 "docker swarm join --token SWMTKN-1-437tkmle44cdhr8atwgdtymol7797dcigdxle6xg535t7d1d83-cualrdhrptlwkp3rhs1hvkm15 192.168.99.100:2377"
    This node joined a swarm as a worker.

    [root@i24 ~]# docker node ls
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
    hq2fc9aib0atrvhf3yf5gg6ii *   mypsqlvm1           Ready               Active              Leader
    ww8ykk0vmne8svgoa2ho8ne21     mypsqlvm2           Ready               Active              

Start the stack talking to "mypsqlvm1" or better called "Swarm manager".

    [root@i24 ~]# docker stack deploy -c docker-compose-PSQL.yml psqladriapp
    Creating network psqladriapp_psqlnet
    Creating service psqladriapp_visualizer
    Creating service psqladriapp_psql

    [root@i24 ~]# docker stack ls 
    NAME                SERVICES
    psqladriapp         2

    [root@i24 ~]# docker stack ps psqladriapp 
    ID                  NAME                       IMAGE                             NODE                DESIRED STATE       CURRENT STATE              ERROR               PORTS
    w6koj18l7zg7        psqladriapp_psql.1         xdri97/postgresql:cloud           mypsqlvm2           Running             Preparing 23 seconds ago                       
    2re85q4ltxu6        psqladriapp_visualizer.1   dockersamples/visualizer:stable   mypsqlvm1           Running             Running 3 seconds ago                          
    i830i6s56f54        psqladriapp_psql.2         xdri97/postgresql:cloud           mypsqlvm1           Running             Preparing 23 seconds ago                       
    8drzohyoprnj        psqladriapp_psql.3         xdri97/postgresql:cloud           mypsqlvm2           Running             Preparing 23 seconds ago                       

    [root@i24 ~]# docker service ps psqladriapp_psql 
    ID                  NAME                 IMAGE                     NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
    w6koj18l7zg7        psqladriapp_psql.1   xdri97/postgresql:cloud   mypsqlvm2           Running             Running 16 seconds ago                       
    i830i6s56f54        psqladriapp_psql.2   xdri97/postgresql:cloud   mypsqlvm1           Running             Running 13 seconds ago                       
    8drzohyoprnj        psqladriapp_psql.3   xdri97/postgresql:cloud   mypsqlvm2           Running             Running 16 seconds ago
    
Asking to vm's IP's:

    [root@i24 ~]# psql -h 192.168.99.100 -U docker -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

    [root@i24 ~]# psql -h 192.168.99.101 -U docker -d lab_clinic -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)
    
Stop all:

    [root@i24 ~]# docker stack rm psqladriapp 
    Removing service psqladriapp_psql
    Removing service psqladriapp_visualizer
    Removing network psqladriapp_psqlnet

    [root@i24 ~]# eval $(docker-machine env -u)

    [root@i24 ~]# docker-machine ssh mypsqlvm2 "docker swarm leave"
    Node left the swarm.

    [root@i24 ~]# docker-machine ssh mypsqlvm1 "docker swarm leave --force"
    Node left the swarm.

    [root@i24 ~]# docker-machine rm mypsqlvm1
    About to remove mypsqlvm1
    WARNING: This action will delete both local reference and remote instance.
    Are you sure? (y/n): y
    Successfully removed mypsqlvm1

    [root@i24 ~]# docker-machine rm mypsqlvm2
    About to remove mypsqlvm2
    WARNING: This action will delete both local reference and remote instance.
    Are you sure? (y/n): y
    Successfully removed mypsqlvm2

### This is the How To PSQL on:
* Deattached Docker.
* Swarm with physical host.
* Swarm with 2 Virtual Machines.

### Hope you enjoyed!
