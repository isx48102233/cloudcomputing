# Cloud computing
_Tecnologies: Docker Swarm, machine, compose, AWS_

Implementar amb tecnologies Docker i un servei extern tipus AWS / DigitalOcean exemples de cloud computing implementant serveis escalables web, postgres, ldap,etc (dels usats al llarg de curs). 

1. Implementar amb containers Docker servidors que funcionin en mode deatach de almenys:
    * LDAP
    * Postgres

2. Estudiar / exposar el funcionament de Docker Compose.
3. Estudiar / exposar el funcionament de Docker Swarm.
4. Estdiar / Exposar el funcionament de Docker Machine

-----------------------------------------------------------------

5. Explorar el funcionament de AWS Amazon Web Services.
6. Observar altres serveis de Clud computing com DigitalOcean i Google Cloud.

-----------------------------------------------------------------

7. Estudiar / exposar el funcionament de Docker Cloud Posar en marxa un cas pràctic (amb els containers treballats) en la infraestructura de Docker Cloud.

8. Integració de Docker Cloud i AWS.


