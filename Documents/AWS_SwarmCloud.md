# Swarm with AWS Intances.

As i did on [this tutorial](https://gitlab.com/isx48102233/cloudcomputing/blob/master/Documents/AWS_Instance.md#now-well-create-a-free-tier-instance), we have to create 2 two Instances on AWS. I have one built right now so just have to create another one. Follow the steps of the link to open ports for services (in my deploying case, of course, you can open others too). I used the same Security Group on both Instances, to open same ports. You can make it as you want!

Select the ssh key (I selected also the same on both Instances).

Now prepare the machine to be able to use Docker.

    [isx48102233@i11 cloudcomputing]$ ssh -i "fromi11.pem" ec2-user@ec2-35-177-33-133.eu-west-2.compute.amazonaws.com
    
    [ec2-user@ip-172-31-24-232 ~]$ sudo yum update -y

    [ec2-user@ip-172-31-24-232 ~]$ sudo yum install docker -y
    
    [ec2-user@ip-172-31-24-232 ~]$ sudo service docker start
    Starting cgconfig service:                                 [  OK  ]
    Starting docker:	                                       [  OK  ]

    [ec2-user@ip-172-31-24-232 ~]$ sudo usermod -aG docker ec2-user

    [ec2-user@ip-172-31-24-232 ~]$ sudo docker version
    Client:
     Version:	17.12.1-ce
     API version:	1.35
     Go version:	go1.9.4
     Git commit:	3dfb8343b139d6342acfd9975d7f1068b5b1c3d3
     Built:	Tue Apr  3 23:37:44 2018
     OS/Arch:	linux/amd64

    Server:
     Engine:
      Version:	17.12.1-ce
      API version:	1.35 (minimum version 1.12)
      Go version:	go1.9.4
      Git commit:	7390fc6/17.12.1-ce
      Built:	Tue Apr  3 23:38:52 2018
      OS/Arch:	linux/amd64
      Experimental:	false

    [ec2-user@ip-172-31-24-232 ~]$ sudo curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
    100   617    0   617    0     0   1209      0 --:--:-- --:--:-- --:--:--  1209
    100 10.3M  100 10.3M    0     0  4082k      0  0:00:02  0:00:02 --:--:-- 6886k

    [ec2-user@ip-172-31-24-232 ~]$ sudo chmod +x /usr/local/bin/docker-compose
    
    [ec2-user@ip-172-31-24-232 ~]$ docker-compose --version
    docker-compose version 1.21.0, build 5920eb0
    
    [ec2-user@ip-172-31-24-232 ~]$ base=https://github.com/docker/machine/releases/download/v0.14.0 && sudo curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine && sudo install /tmp/docker-machine /usr/local/bin/docker-machine
    % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
    100   617    0   617    0     0   1277      0 --:--:-- --:--:-- --:--:--  1277
    100 26.7M  100 26.7M    0     0  1924k      0  0:00:14  0:00:14 --:--:-- 2137k

    [ec2-user@ip-172-31-24-232 ~]$ docker-machine version
    docker-machine version 0.14.0, build 89b8332

I will deploy a stack only on the new Instance, to test that it works as expected.

    [ec2-user@ip-172-31-24-232 ~]$ sudo docker swarm init

    [ec2-user@ip-172-31-24-232 ~]$ sudo docker stack deploy -c docker-compose-cloud.yml AmazonCloud
    Creating network AmazonCloud_netadri
    Creating network AmazonCloud_default
    Creating service AmazonCloud_portainer
    Creating service AmazonCloud_ldap
    Creating service AmazonCloud_psql
    Creating service AmazonCloud_visualizer

    [ec2-user@ip-172-31-24-232 ~]$ sudo docker stack ls
    NAME                SERVICES
    AmazonCloud         4

    [ec2-user@ip-172-31-24-232 ~]$ sudo docker stack ps AmazonCloud
    ID                  NAME                       IMAGE                             NODE                DESIRED STATE       CURRENT STATE                ERROR               PORTS
    ew4c35ihcu9g        AmazonCloud_visualizer.1   dockersamples/visualizer:stable   ip-172-31-24-232    Running             Running 28 seconds ago                           
    1d40o1r5i4w2        AmazonCloud_psql.1         xdri97/postgresql:cloud           ip-172-31-24-232    Running             Running 35 seconds ago                           
    vf6k9800hgxd        AmazonCloud_ldap.1         xdri97/ldapserver:cloud           ip-172-31-24-232    Running             Running 36 seconds ago                           
    721r9er0r2rw        AmazonCloud_portainer.1    portainer/portainer:latest        ip-172-31-24-232    Running             Running about a minute ago                       

    [ec2-user@ip-172-31-24-232 ~]$ sudo docker service ls
    ID                  NAME                     MODE                REPLICAS            IMAGE                             PORTS
    745t8xixkn2t        AmazonCloud_ldap         replicated          1/1                 xdri97/ldapserver:cloud           *:389->389/tcp
    4ptpdiprxpox        AmazonCloud_portainer    replicated          1/1                 portainer/portainer:latest        *:9000->9000/tcp
    nq1dkuup341x        AmazonCloud_psql         replicated          1/1                 xdri97/postgresql:cloud           *:5432->5432/tcp
    xsaandohljaa        AmazonCloud_visualizer   replicated          1/1                 dockersamples/visualizer:stable   *:8080->8080/tcp

Test all services. Commands are from the first instance I made:

    [ec2-user@ip-172-31-29-231 ~]$ ldapsearch -x -LLL -h 35.177.33.133 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

    [ec2-user@ip-172-31-29-231 ~]$ psql -U docker -d lab_clinic -h 35.177.33.133 -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

![Worker Portainer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/workerportainer.png)

![Worker Visualizer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/workervisualizer.png)

**Note**: Portainer and Visualizer (and all services) can be visited by Public AWS DNS instead of Public IP.

Now stop services and swarm:

    [ec2-user@ip-172-31-24-232 ~]$ sudo docker stack rm AmazonCloud
    Removing service AmazonCloud_ldap
    Removing service AmazonCloud_portainer
    Removing service AmazonCloud_psql
    Removing service AmazonCloud_visualizer
    Removing network AmazonCloud_netadri
    Removing network AmazonCloud_default

    [ec2-user@ip-172-31-24-232 ~]$ sudo docker swarm leave --force
    Node left the swarm.

### Now that we have verified that the instance is working correctly, let's create the _Cloud Swarm_.

We have to be able to log into both instances by SSH. AWS web ( Instances section) should look like this:

![2 AWS Instances](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/2awsinstances.png)

Security Group have to be changed too. Swarm use port 2377 to connect workers with managers.
To use the ingress network in the swarm ([_swarm mode routing mesh_](https://gitlab.com/isx48102233/cloudcomputing/blob/master/Documents/CloudComputing.md#qu%C3%A8-es-el-routing-mesh)), you need to have the following ports open between the swarm nodes before you enable swarm mode:
* Port 7946 TCP/UDP for container network discovery.
* Port 4789 UDP for the container ingress network.

Security group should look like this:

![Securirty Group Swarm](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/securitygroupswarm.png)

Before anything I will specify something:

* MANAGER: [ec2-user@ip-172-31-29-231 ~]$
* WORKER: [ec2-user@ip-172-31-24-232 ~]$

Ok, so. Let's start:

**Create swarm:**

    [ec2-user@ip-172-31-29-231 ~]$ sudo docker swarm init
    Swarm initialized: current node (ahblj3ge75k8x5r8raf396dkz) is now a manager.
    To add a worker to this swarm, run the following command:
    docker swarm join --token SWMTKN-1-5kft04erfg3nxrdzgrwyx946rvhi5lcorpcaid9fq3yylf3fm8-8wpbrxfzk4gw0vegspzpv6a4q 172.31.29.231:2377
    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
    
**Make worker join swarm:**

    [ec2-user@ip-172-31-24-232 ~]$ docker swarm join --token SWMTKN-1-5kft04erfg3nxrdzgrwyx946rvhi5lcorpcaid9fq3yylf3fm8-8wpbrxfzk4gw0vegspzpv6a4q 172.31.29.231:2377
    This node joined a swarm as a worker.

    
**See nodes that build the swarm:**

    [ec2-user@ip-172-31-29-231 ~]$ sudo docker node ls
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
    pjz46svhafi226ku2p8ws8wls     ip-172-31-24-232    Ready               Active              
    ahblj3ge75k8x5r8raf396dkz *   ip-172-31-29-231    Ready               Active              Leader

    
**Deploy the application. There will be 4 services with only 1 replica. Anyways we will be able to ask both IP's even if they don't contain a service replica:**

    [ec2-user@ip-172-31-29-231 ~]$ sudo docker stack deploy -c docker-compose-cloud.yml AWSCloud
    Creating network AWSCloud_netadri
    Creating network AWSCloud_default
    Creating service AWSCloud_psql
    Creating service AWSCloud_visualizer
    Creating service AWSCloud_portainer
    Creating service AWSCloud_ldap
    
**See the stack status:**

    [ec2-user@ip-172-31-29-231 ~]$ sudo docker stack ls
    NAME                SERVICES
    AWSCloud            4

    [ec2-user@ip-172-31-29-231 ~]$ sudo docker stack ps AWSCloud
    ID                  NAME                    IMAGE                             NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
    z7exv0ve4k8v        AWSCloud_ldap.1         xdri97/ldapserver:cloud           ip-172-31-24-232    Running             Running 15 minutes ago                       
    m11pqhkuy24s        AWSCloud_portainer.1    portainer/portainer:latest        ip-172-31-29-231    Running             Running 15 minutes ago                       
    sufh21q6zulg        AWSCloud_visualizer.1   dockersamples/visualizer:stable   ip-172-31-29-231    Running             Running 15 minutes ago                       
    wxg6u8xhovpx        AWSCloud_psql.1         xdri97/postgresql:cloud           ip-172-31-29-231    Running             Running 15 minutes ago      

**See the service status:**

    [ec2-user@ip-172-31-29-231 ~]$ sudo docker service ls
    ID                  NAME                  MODE                REPLICAS            IMAGE                             PORTS
    zhc0a072wztx        AWSCloud_ldap         replicated          1/1                 xdri97/ldapserver:cloud           *:389->389/tcp
    v8s8rzynb3ca        AWSCloud_portainer    replicated          1/1                 portainer/portainer:latest        *:9000->9000/tcp
    bopb7v6j6uf7        AWSCloud_psql         replicated          1/1                 xdri97/postgresql:cloud           *:5432->5432/tcp
    yld166oedgc8        AWSCloud_visualizer   replicated          1/1                 dockersamples/visualizer:stable   *:8080->8080/tcp

### Testing

**PSQL**

Manager IP

    [isx48102233@i11 cloudcomputing]$ psql -U docker -d lab_clinic -h 35.178.66.75 -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

Worker IP

    [isx48102233@i11 cloudcomputing]$ psql -U docker -d lab_clinic -h 18.130.14.207 -c "select * from doctors;"
    Password for user docker: 
    iddoctor |  nom   | cognoms  | especialitat 
    ----------+--------+----------+--------------
            1 | albert | marinom  | cirugia
            2 | maria  | benavent | podologia
            3 | renzo  | remar    | fisioterapia
            4 | adri   | davila   | fisioterapia
    (4 rows)

**VISUALIZER**

Manager IP

![Manager Visualizer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/awsmanagervisualizer.png)

Worker IP

![Worker Visualizer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/awsworkervisualizer.png)

**LDAP**

Manager IP

    [isx48102233@i11 cloudcomputing]$ ldapsearch -x -LLL -h 35.178.66.75 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org
    
Worker IP

    [isx48102233@i11 cloudcomputing]$ ldapsearch -x -LLL -h 18.130.14.207 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

**PORTAINER**

Manager IP

![Manager Portainer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/awsmanagerportainer.png)

Worker IP

![Worker Portainer](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/awsworkerportainer.png)

### And this is How To Create a swarm on/with:
* AWS Instances.
* LDAP, PSQL, Visualizer and Portainer services.
* Using ingress network

![Ingress Network](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/ingressnetwork.png)

### Hope you enjoyed!

### Hope you enjoyed!