# Quin és el significat de Cloud Computing?

Explicat senzillament, el Cloud Computing es basa en recursos compartits, programari i tot tipus d'informació disponibles des de qualsevol punt del món, a qualsevol hora i sense haver de desplaçar-se físicament.
Avui en dia aquests conceptes poden ser desconeguts com a concepte, però el tenim interioritzat i l'utilitzem diàriament. 
Qualsevol canal que ens permet tornar a reproduir el nostre programa televisiu favorit fent 4 clics a la seva pàgina web ja ens proporciona un servei, oferint els seus recursos multimèdia sense haver de descarregar cap vídeo localment.

Altres conceptes com el Cloud Storage estan molt lligats al Cloud Computing. 
Amb aquests noms potser no els reconeixem tan fàcilment com si diem "Dropbox" o "Google Drive". 
Per això és que el concepte d'emmagatzament al núvol no se sap explicar en l'àmbit d'usuari quotidià però tothom sap per a què serveix.

La definició tècnica seria que la informàtica en el núvol és el lliurament sota demanda de potència informàtica, emmagatzematge en bases de dades, aplicacions i altres recursos de TI a través d'Internet amb un sistema de preus basat en el consum realitzat.

# Què són els Containers?

Els containers són espais aïllats generats pel kernel linux que permeten l'execució d'aplicacions aïllades entre si. 
Per tant, en cap cas, l'execució d'una aplicació a un container afectarà l'execució d'una altra aplicació a un altre container.

Aquests containers són utilitzats en major part per a entorns de proves, d'un nou sistema operatiu, una nova configuració per al teu servidor web, etc. ja que la seva producció és molt més ràpida i ocupa menys espai que una màquina virtual.
Com veiem a la imatge, una VM carrega un Sistema Operatiu, mentre que els Containers de Docker no.

![Comparació Containers vs VM's](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/dockervsvm.png)

# Docker

Docker és la companyia que porta el moviment dels containers i l'únic proveïdor de plataforma de contenidors per fer front a totes les aplicacions a través del núvol híbrid. 

La migració de núvols, la infraestructura de núvols múltiples o híbrides requereixen una portabilitat sense friccions de les aplicacions.
Les aplicacions de paquets Docker i les seves dependències es combinen en un contenidor aïllat fent-les portar a qualsevol infraestructura.
L'objectiu és eliminar el problema anomenat "funciona a la meva màquina".
La infraestructura certificada per Docker garanteix que les aplicacions en contenidor funcionen de forma consistent.

Docker permet crear containers amb els nostres serveis a partir d'imatges base o d'imatges ja preparades per al funcionament del servei.
Durant tot el projecte parlaré de 4 serveis:
* PostgreSQL
* Openldap
* Visualizer
* Portainer

"Físicament", no és necesita cap docker funcionant per a generar clústers. 
És a dir, a la nostra màquina no necesitem tenir cap container encès, ni tan sols tenir cap imatge descarregada, ja que tal com s'ha muntat tot les imatges estan allotjades a *Docker Hub* i els containers es generen automàticament segons la configuració del stack.

# Serveis

Sovint, un servei és la imatge d'un servei de micro servei en el context d'una aplicació més gran. 
Els exemples de serveis poden incloure un servidor HTTP, una base de dades o qualsevol altre tipus de programa executable que vulgueu executar en un entorn distribuït.

Per tant, cada servei que incloem al Docker Compose, generarà 1 o N tasques.
Podríem dir que tasques és el mateix que repliques.

![Diagrama Serveis](https://gitlab.com/isx48102233/cloudcomputing/raw/master/images/diagramaserveis.png)

## PostgreSQL

Aquest servei consta d'un servidor PostgreSQL amb una base de dades feta a classe anomenada **lab_clinic**. L'usuari per accedir-hi es diu **docker** i el seu password és **jupiter**.

## Openldap

Aquest servei consta d'un servidor openldap amb una organització amb **dn: dc=edt,dc=org** anomenada **Escola del treball de Barcelona**. També conté alguna organització i un grapat d'usuaris.

## Visualizer

Aquest és un servei proporcionat per Docker que permet controlar els serveis i tasques que s'executen. El seu port és el 8080.

## Portainer

Aquest servei és el més complet. 
Permet controlar, fer i esborrar tot el relacionat amb Docker. 
Veure xarxes, containers, imatges, stacks, serveis... 
També pots veure qui està utilitzant cada element, eliminar o crear d'altres. 
El port utilitzat és el 9000.

# Imatges

Les imatges utilitzades estan totes al núvol, no cal ni tan sols descarregar-les previament. 
Dues són propies creades amb Dockerfile i les altres són proporcionades per externs.

* PostgreSQL:   xdri97/postgresql:cloud
    * La creació d'aquesta imatge passa pel dockerfile allotjat en [aquest repositori](https://gitlab.com/isx48102233/cloudcomputing/tree/master/PostgreSQL).
* Openldap:     xdri97/ldapserver:cloud
    * La creació d'aquesta imatge passa pel dockerfile allotjat en [aquest repositori](https://gitlab.com/isx48102233/cloudcomputing/tree/master/Ldapserver).
* Visualizer:   dockersamples/visualizer:stable
* Portainer:    portainer/portainer

# Docker + Cloud

I ara entrem en matèria. Què és un servei? Com es crea? Què és un swarm? Per a què serveixen docker compose, docker-machine i totes aquestes eines?
Abans però, per posar en pràctica totes aquestes característiques de Docker, s'han d'instal·lar. Deixo un [script](https://gitlab.com/isx48102233/cloudcomputing/blob/master/updates.sh) per a fer totes les instal·lacions necessàries a un Fedora 24.

_Aquest script desinstal·larà la versió actual de Docker, per tant, guardar a **Docker Hub** les imatges importants que tinguis localment seria la millor idea._

## Docker Compose

Docker Compose és una eina Docker per a definir i executar aplicacions multi-container. Compose utilitza un fitxer YAML per a definir els serveis que volem crear dins de la nostra aplicació.
Tenint aquest fitxer i amb una sola comanda, farem que la nostra aplicació començi a funcionar (si tot está ben configurat, és clar!).

### En quins casos s'utilitza Compose?

* En entorns de desenvolupament; on podem testejar configuracions que no afectin màquines físiques.
* Entorns de proves automàtics; quan només en tenim un servei (com el comptador de visites del get started), fent _docker-compose up_ et genera l'entorn de proves per testejar. 
* En el meu cas, de Compose només utilitzarem la part del fitxer YAML.
* Implementacions d'un host únic; Es pot utilitzar Compose per implementar en un motor Docker remot.

### Instal·lació

Per defecte la instal·lació de Docker no instal·la Compose. A l'[script](https://gitlab.com/isx48102233/cloudcomputing/blob/master/updates.sh) explico com instal·lar-ho automàticament.
Cal dir, que per fer funcionar Compose i altres eines similars cal actualitzar Docker a una versió com la Docker CE (17.07), ja que per defecte l'ordre _dnf install docker_ instal·la una versió molt més antiga que no suporta aquestes eines
Partint d'aquesta base, així s'instal·la i es comprova que hi es Docker Compose:

    # Install Docker Compose
    curl -L https://github.com/docker/compose/releases/download/1.21.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    # Comprovació
    echo "Versió de Docker Compose"
    echo "----------------"
    docker-compose --version
    echo "----------------"
    
### Fitxer Compose

El fitxer Compose defineix Serveis, Xarxes i Volums.
La forma més ràpida per crear stacks i swarms és utilitzant un fitxer YAML amb la configuració dels serveis. L'ordre _docker stack_ accepta imatges ja creades per fer _deploy_.
Cada definició de servei, amb la seva configuració, afecta els containers generats automàticament per a assegurar l'execució d'aquest. 
És a dir, fa els _docker container create_ necessaris per a posar-se en marxa.
Ja sabem la utilitat de docker compose i el seu fitxer YAML, però jo l'utilitzaré per a crear stacks en mode swarm. És per això que utilitzarem les opcions:
* SERVICES
* NETWORKS

Dins d'aquestes categories es situen els respectius noms:

    services:
      ldap:
        [...]
      psql
        [...]
      visualizer
        [...]
    networks:
      netadri:

Seguidament, i al lloc on hi ha "[...]", és el torn de les opcions de configuració de cada servei on trobarem les següents, amb diferents atributs i valors:
* image
* environment
* deploy
* ports
* networks
* volumes

### Ordres

|**Ordre**|**Utilitat**|
|--------|--------|
|docker stack deploy -c _composefile.yml_ _stackname_|Crea els serveis del fitxer compose a un stack anomenat _stackname_|
|docker stack ls|Mostra tots els stacks i la quantitat de serveis que tenen|
|docker stack ps _stackname_|Mostra de les tasques, id, nom, imatge utilitzada, estat, el node on estàn allotjades i quant temps fa que funcionen|
|docker service ls|De tots els serveis existents mostra: id, nom, mode, quantitat de repliques, imatge utilitzada i ports exposats|
|docker service rm _servicename_|Esborra el servei especificat|
|docker stack rm _stackname_|Esborra tots els serveis pertanyents a l'_stackname_|
    
## Docker Swarm

Un Swarm és un conjunt de màquines que executen Docker i que formen un clúster. 
Les ordres docker són les mateixes però ara aniran dirigides al Swarm manager. 
Les màquines d'un swarm poden ser físiques o virtuals. 
Després d'unir-se a un swarm, les màquines es denominen nodes.

Els swarm managers només son màquines que executen les ordres docker i són qui autoritzen a altres màquines a entrar al Swarm com a "Workers". 
Els workers serveixen per a donar capacitat, ja que no tenen cap autoritat per autoritzar res.

No és el mateix un Swarm a el swarm mode:
* Swarm és el concepte, l'estructura.
* Swarm mode és l'execució, la creació d'un swarm i la seva utilització.

### Ordres

|**Ordre**|**Utilitat**|
|--------|--------|
|docker swarm init|Iniciar un swarm|
|docker swarm init --advertise-addr _IP_|Iniciar un swarm especificant la ip on hi serà el manager|
|docker swarm join --token _TOKEN_ _IP_:2377|Accedir a un swarm com a worker|
|docker swarm leave|Deixar un swarm com a worker|
|docker swarm leave --force|Deixar un swarm com a manager|
|docker node ls|_Note: Només al swarm manager_ Printa els nodes pertanyents al swarm, el seu estat, disponibilitat i versió de docker|

### Caracteristiques

#### Què és el Routing Mesh?

El routing mesh fa que cada node pertanyent al swarm accepti connexions als ports publicats per a qualsevol servei funcionant al swarm, malgrat que el node no tingui una tasca funcionant en el dit node. Routing Mesh encamina totes les peticions entrants als ports publicats cap a un container actiu.

## Docker Machine 

Machine és una eina Docker que permet la instal·lació del motor Docker en màquines virtuals que resideixen al teu host, al cloud o a servers físics. 
Aquestes màquines són controlades, i creades, amb les ordres derivades de _docker-machine_.
Per tant amb Docker Machine s'habilita la possibilitat de crear imatges i containers a un entorn probablement de proves com pot ser una màquina virtual.

### Instal·lació

Per defecte la instal·lació de Docker no instal·la Machine, l'eina que s'utilitza per a swarms. A l'[script](https://gitlab.com/isx48102233/cloudcomputing/blob/master/updates.sh) explico com instal·lar-ho automàticament.
Cal dir, que per fer funcionar Machine i altres eines similars cal actualitzar Docker a una versió com la Docker CE (17.07), ja que per defecte l'ordre _dnf install docker_ instal·la una versió molt més antiga que no suporta aquestes eines
Partint d'aquesta base, així s'instal·la i es comprova que hi és Docker Machine:

    # Install Docker Machine
    base=https://github.com/docker/machine/releases/download/v0.14.0 && curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine && sudo install /tmp/docker-machine /usr/local/bin/docker-machine
    echo "Versió de Docker Machine"
    echo "----------------"
    docker-machine version
    echo "----------------"

### Drivers

#### VirtualBox 

Les màquines virtuals s'han de fer amb algun driver, i el més comú és VirtualBox. Recomano desinstal·lar tot el que tinguis de VirtualBox per instal·lar-ho sense problemes. A més s'ha d'actualitzar el kernel perquè tot funcioni:

    # Install VirtualBox
    wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo -O /etc/yum.repos.d/virtualbox.repo
    dnf -y install VirtualBox-5.1
    # Kernel per que VirtualBox funcioni sense errors
    dnf install -y kernel-headers kernel-devel dkms gcc
    /sbin/vboxconfig
    
#### AWS

Seguint els passos d'[aquest](https://docs.docker.com/machine/examples/aws/) tutorial aconseguirem crear una instància EC2 d'AWS dockeritzada utilitzant docker-machine amb el driver **amazonec2**. 

#### DigitalOcean

Seguint els passos d'[aquest](https://docs.docker.com/machine/examples/ocean/) tutorial aconseguirem crear un Droplet de DigitalOcean dockeritzat utilitzant docker-machine amb el driver **digitalocean**. 

### Ordres

|**Ordre**|**Utilitat**|
|--------|--------|
|docker-machine create --driver virtualbox _vmname_|Crea una maquina virtual que contingui el motor Docker amb el driver de virtualbox i anomenada _vmname_|
|docker-machine start _vmname_|Iniciar una màquina virtual|
|docker-machine stop _vmname_|Apagar una màquina virtual|
|docker-machine rm _vmname_|Esborrar una màquina virtual|
|docker-machine ssh _vmname_ "ordre docker"|Executar una ordre docker a una màquina virtual|
|eval $(docker-machine env _vmname_)|Configura el shell per parlar directament a la màquina virtual|
|eval $(docker-machine env -u)|Treu la connexió de shell|

# Proveidors de Serveis Cloud

## Amazon Web Services

Amazon Web Services (AWS) és una filial d'Amazon que proporciona plataformes de computació en xarxa sota demanda per a particulars, empreses i governs, amb subscripció pagada.
La tecnologia permet als subscriptors disposar d'un ampli clúster virtual d'ordinadors, disponible tot el temps, a través d'Internet. 
La versió AWS d'ordinadors virtuals té la majoria dels atributs d'una computadora real.
Una elecció de sistemes operatius, networking i programari de càrrega precarregat, com ara servidors web, bases de dades, CRM, etc.

Com a part de l'acord de subscripció, Amazon gestiona, actualitza i proporciona seguretat estàndard a la indústria del sistema de subscriptors.
AWS opera des de moltes regions geogràfiques globals, incloses 6 a Amèrica del Nord.

Les utilitzades a Europa són: Londres, París, Frankfurt i Irlanda.

Algun dels serveis d'AWS són:
* Memòria al núvol: permet d'emmagatzemar tot tipus de dades a servidors d'internet.
* Buscador al núvol: permet de realitzar recerques.
* Base de dades Dynamo: o DDB de tipus NoSQL.
* Màquines virtuals on poder executar les aplicacions.
* Eines d'anàlisi i mètriques per a mesurar tot tipus d'estadística.

Les màquines virtuals que proporciona AWS s'anomenen instàncies.

AWS és considerada l'empresa pionera en donar els serveis esmentats. 
Per això l'he escollit.

### Comparacions

Quin ha de ser el detall que ens faci escollir un proveïdor o un altre? 
Doncs molt senzill, segons la utilitat que vulguis donar, els recursos que necessites i el cost que pots assumir.
Llavors, hi ha dos grans grups els quals ens podem ajudar a escollir: 
* Propòsits generals
* Costos reduïts.

#### Propòsits generals

Un núvol de propòsit general està destinat a executar qualsevol cosa. 
Pot substituir un bastidor complet de servidors, ja que pot reemplaçar un centre de dades sencer. 
És la solució de gestió per executar moltes aplicacions heterogènies que requereixen una gran varietat de maquinari. 
La versatilitat la fa ideal per executar una operació sencera al núvol. 
És un ajustament perfecte per a tota una empresa tecnològica o qualsevol projecte tecnològic.

Què ens pot fer saber que és el nostre cas? Si contestaríem **si** a qualsevol d'aquestes preguntes:

* Executes més de 50 màquines virtuals?
* Estàs gastant més de 1000 dòlars / mes en allotjament?
* La vostra infraestructura abasta diversos centres de dades?

En aquest àmbit hi juguen empreses com AWS, Google Cloud i Microsoft Azure com a grans destacats.

##### Selecció de proveïdor

El núvol de Google és molt superior als seus competidors. És més barat, més ràpid i més fàcil de gestionar. Si aneu al núvol, aneu **Google Cloud**. 
AWS té el doble de preu per executar la mateixa infraestructura, a més de ser més lenta i amb menys capacitats. 

Corres amb els diners de la gran empresa i prefereixes jugar amb la gestió segura? (**AWS**) o corres amb fons limitats i preferiu anar amb la millor plataforma? (**Google**). 

Aquest és l'estat actual d'AWS contra Google Cloud.

#### Costos reduïts

Un núvol de costos reduïts està destinat a oferir servidors adequats a les masses, on adequat vol dir un maquinari decent i una bona connectivitat a Internet, a un preu assequible. 
És la solució d'accés a totes les necessitats bàsiques. 
Per exemple, els professionals que treballen amb uns serveis senzills amb trànsit poc moderat, les agències necessiten un allotjament senzill per retornar al client i aficionats que realitzen experiments. 
En termes generals, és la millor opció per a qualsevol que estigui cercant un parell de servidors, especialment si el criteri és:

* Fàcil de gestionar
* Bons resultats

Què ens pot fer saber que és el nostre cas? Si contestaríem **si** a qualsevol d'aquestes preguntes:

* Executes menys de 5 màquines virtuals?
* Estàs gastant menys de 100 dòlars / mes en allotjament?
* T'etiquetaries com a amateur o aficionat?

##### Selecció de proveïdor

Per a aquest propòsit, el proveïdor indicat es **Digital Ocean**. Linode podria ser una opció però Digital Ocean té molt aventatge en aquest sector.
DigitalOcean afirma que els seus "Droplets", el seu terme per cridar als servidors en el núvol, poden ser subministrats en 55 segons. 
La companyia també proporciona discos dur SSD i virtualització KVM.