# Link AWS with DockerCloud

Hi! I will show some steps to connect a AWS Account with Docker Cloud.

### AWS
1. Create the account
2. Log in
3. Go to the AWS IAM Role creation panel at [Click](https://console.aws.amazon.com/iam/home#roles). Click Create role.
4. Select Another AWS account to allow your Docker Cloud account to perform actions in this AWS account.
5. In the Account ID field, enter the ID for the Docker Cloud service: 689684103426
6. Select Require external ID (Best practice when a third party will assume this role).
    * In the External ID field, enter the namespace to link. In my case "xdri97"
7. Click Next: Review.
8. Give the new role a name. In my case: "dockercloud-swarm-role".
9. Click Create Role.
10. Click the name of the role you just created to view its details.
11. On the Permissions tab, click + Add an inline policy.
12. Choose the JSON tab.
13. Copy and paste the policy document found in the [Docker for AWS page](https://docs.docker.com/docker-for-aws/iam-permissions/).
14. Click Review Policy. The policy validator reports any syntax errors. I used a name for the policy like this "dockercloud-swarm-policy"
15. Click Create Policy to save your work.
16. Back on the role view, click into the new role to view details, and copy the full Role ARN string.

### Docker cloud

1. In Docker Cloud, click the account menu at the upper right and select Cloud settings.
2. In the Service providers section, click the plug icon next to Amazon Web Services.
3. Enter the full Role ARN for the role you just created.
4. Click Save.
