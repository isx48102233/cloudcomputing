# How to deploy a LDAP service on swarm

First, we have our ldap directory with all the config files and Dockerfile:

    [root@i24 ldapproves]# ls
    Dockerfile  install.sh  ldap.conf  organitzacio-edt.org.ldif  slapd.conf  startup.sh  usuaris-edt.org.ldiff

Let's create the image with Dockerfile with _docker build_.

    [root@i24 ldapproves]# docker build -t xdri97/ldap:prova .

Now we have a ldap image that can run ldap on deattached mode. _Remember to use -p to publish the port_.

    [root@i24 ldapproves]# docker run -d -p 389:389 xdri97/ldap:prova
    b9a8afb423ad3c24390f4a1357e67b4a5d112ef59dca2cf72ad23c094e8dbed6

Ask to ldap docker with ldapsearch:

    [root@i24 ~]# ldapsearch -x -LLL -h 172.17.0.2:389 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

It works. Now I will build another image to push it into DockerHub:

    [root@i24 ldapproves]# docker build -t xdri97/ldapserver:cloud .
    [root@i24 ldapproves]# docker push xdri97/ldapserver:cloud 

Initialize the swarm

    [root@i24 ldapproves]# docker swarm init
    Swarm initialized: current node (jjraiuzhdddb72tdh2z5rv0jk) is now a manager.
    To add a worker to this swarm, run the following command:
    docker swarm join --token SWMTKN-1-4a60xb9d0x3ci19ifcbwcar31ga1snvmcei6c54dmk90ki155f-69iljwfkuxjq7rur3xljh1qdq 192.168.2.54:2377
    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

Let's create the stack and test it:

    [root@i24 ~]# docker stack deploy -c docker-compose-LDAP.yml ldapadriapp
    Creating network ldapadriapp_webnet
    Creating service ldapadriapp_web

    [root@i24 ~]# docker service ls
    ID                  NAME                MODE                REPLICAS            IMAGE                     PORTS
    ewc33lznqer1        ldapadriapp_web     replicated          5/5                 xdri97/ldapserver:cloud   *:389->389/tcp

    [root@i24 ~]# docker service ps ldapadriapp_web 
    ID                  NAME                IMAGE                     NODE                                   DESIRED STATE       CURRENT STATE            ERROR               PORTS
    czktekzvn5bi        ldapadriapp_web.1   xdri97/ldapserver:cloud   i24.informatica.escoladeltreball.org   Running             Running 25 seconds ago                       
    nij9nyvvwque        ldapadriapp_web.2   xdri97/ldapserver:cloud   i24.informatica.escoladeltreball.org   Running             Running 25 seconds ago                       
    1d5w5kegn3zz        ldapadriapp_web.3   xdri97/ldapserver:cloud   i24.informatica.escoladeltreball.org   Running             Running 25 seconds ago                       
    ov71b07uy8d4        ldapadriapp_web.4   xdri97/ldapserver:cloud   i24.informatica.escoladeltreball.org   Running             Running 25 seconds ago                       
    jtgwbhp1zbat        ldapadriapp_web.5   xdri97/ldapserver:cloud   i24.informatica.escoladeltreball.org   Running             Running 25 seconds ago                       

    [root@i24 ~]# ldapsearch -x -LLL -h i24.informatica.escoladeltreball.org:389 -b "dc=edt,dc=org" -s base
    dn: dc=edt,dc=org
    dc: edt
    description: Escola del treball de Barcelona
    objectClass: dcObject
    objectClass: organization
    o: edt.org

Delete the stack and leave the swarm.

    [root@i24 ~]# docker stack rm ldapadriapp 
    Removing service ldapadriapp_web
    Removing network ldapadriapp_webnet

    [root@i24 ~]# docker swarm leave --force
    Node left the swarm.

Now is time to make the same but with VM's:

    [root@i24 ~]# docker-machine create --driver virtualbox myldapvm1

    [root@i24 ~]# docker-machine create --driver virtualbox myldapvm2

    [root@i24 ~]# docker-machine ls
    NAME        ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myldapvm1   -        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myldapvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce   

Make our machine talk to myldapvm1(manager) as default and use "docker machine ssh myldapvm2" to talk to the worker and make him join the swarm.

    [root@i24 ~]# eval $(docker-machine env myldapvm1)

    [root@i24 ~]# docker-machine ls
    NAME        ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
    myldapvm1   *        virtualbox   Running   tcp://192.168.99.100:2376           v18.04.0-ce   
    myldapvm2   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.04.0-ce   

    [root@i24 ~]# docker swarm init --advertise-addr 192.168.99.100
    Swarm initialized: current node (kyycisxccwrsaudf77e14jndz) is now a manager.
    To add a worker to this swarm, run the following command:
    docker swarm join --token SWMTKN-1-3pfqkheeipo5vz7nt6lh12w3u8ec1krbwypk8kx17lbt4bloki-3f7080zffhirotrque2b634tb 192.168.99.100:2377
    To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

    [root@i24 ~]# docker-machine ssh myldapvm2 "docker swarm join --token SWMTKN-1-3pfqkheeipo5vz7nt6lh12w3u8ec1krbwypk8kx17lbt4bloki-3f7080zffhirotrque2b634tb 192.168.99.100:2377"
    This node joined a swarm as a worker.
    
We still talking to the manager "myldapvm1" so let's see the status of nodes:

    [root@i24 ~]# docker node ls
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
    kyycisxccwrsaudf77e14jndz *   myldapvm1           Ready               Active              Leader
    55bhqfilrt3m525elenstvxyc     myldapvm2           Ready               Active     

To stop talking him run the following command:

    [root@i24 ~]# eval $(docker-machine env -u)
    
Of course now the "docker node ls" dont work because our host isn't a swarm manager so make the next command to do the same:

    [root@i24 ~]# docker node ls
    Error response from daemon: This node is not a swarm manager. Use "docker swarm init" or "docker swarm join" to connect this node to swarm and try again.

    [root@i24 ~]# docker-machine ssh myldapvm1 "docker node ls"
    ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
    kyycisxccwrsaudf77e14jndz *   myldapvm1           Ready               Active              Leader              18.04.0-ce
    55bhqfilrt3m525elenstvxyc     myldapvm2           Ready               Active                                  18.04.0-ce

Again configure env to talk defaultly to manager so we can use local files into VM's. This way we can use docker-compose file in VM having it on our host:

    [root@i24 ~]# eval $(docker-machine env myldapvm1)

    [root@i24 ~]# docker stack deploy -c docker-compose-LDAP.yml ldapadriapp
    Creating network ldapadriapp_webnet
    reating service ldapadriapp_web

    [root@i24 ~]# docker service ps ldapadriapp_web 
    ID                  NAME                IMAGE                     NODE                DESIRED STATE       CURRENT STATE              ERROR               PORTS
    to9zrqrtnbwh        ldapadriapp_web.1   xdri97/ldapserver:cloud   myldapvm1           Running             Preparing 13 seconds ago                       
    yv0mwff0tlvu        ldapadriapp_web.2   xdri97/ldapserver:cloud   myldapvm2           Running             Preparing 14 seconds ago                       
    8vs65og2ipwe        ldapadriapp_web.3   xdri97/ldapserver:cloud   myldapvm2           Running             Preparing 13 seconds ago                       
    88alb0cr8di5        ldapadriapp_web.4   xdri97/ldapserver:cloud   myldapvm1           Running             Preparing 13 seconds ago                       
    ypfdkd0edzyw        ldapadriapp_web.5   xdri97/ldapserver:cloud   myldapvm2           Running             Preparing 13 seconds ago  

Now stop swarm and stack and i also removed vm's:

    [root@i24 ~]# docker stack rm ldapadriapp
    Removing service ldapadriapp_web
    Removing network ldapadriapp_webnet

    [root@i24 ~]# eval $(docker-machine env -u)

    [root@i24 ~]# docker-machine ssh myldapvm2 "docker swarm leave"
    Node left the swarm.

    [root@i24 ~]# docker-machine ssh myldapvm1 "docker swarm leave --force"
    Node left the swarm.

    [root@i24 ~]# docker-machine rm myldapvm1
    About to remove myldapvm1
    WARNING: This action will delete both local reference and remote instance.
    Are you sure? (y/n): y
    Successfully removed myldapvm1

    [root@i24 ~]# docker-machine rm myldapvm2
    About to remove myldapvm2
    WARNING: This action will delete both local reference and remote instance.
    Are you sure? (y/n): y
    Successfully removed myldapvm2
    
### This is the How To LDAP on:
* Deattached Docker.
* Swarm with physical host.
* Swarm with 2 Virtual Machines.

### Hope you enjoyed!

